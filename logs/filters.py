from django_filters import rest_framework as filters
from logs.models import Logs


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class LogFilter(filters.FilterSet):
    date = filters.IsoDateTimeFromToRangeFilter()
    status_code__in = NumberInFilter(field_name='status_code', lookup_expr='in')

    class Meta:
        model = Logs
        fields = ['date', 'status_code']
