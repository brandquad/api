from rest_framework import viewsets, mixins, generics
from rest_framework.response import Response
from logs.models import Logs
from logs.serializers import LogSerializer
from django_filters import rest_framework as filters
from logs.filters import LogFilter
from django.db.models import Count, Sum


class LogsViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = LogSerializer
    queryset = Logs.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = LogFilter


class StatsView(generics.GenericAPIView):
    queryset = Logs.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = LogFilter

    def get(self, request):
        # todo Обработка ошибок
        queryset = self.filter_queryset(self.get_queryset())
        method_count = list(queryset.values('method').annotate(count=Count('method')))
        bytes_sent = queryset.aggregate(total=Sum('response_size'))
        ip_count = list(queryset.values('ip').annotate(count=Count('ip')).order_by('-count'))

        return Response(
            data={'method_count': method_count, 'bytes_sent': bytes_sent.get('total'), 'ip_count': ip_count},
            status=200
        )
