from logs.models import Logs
from rest_framework import serializers


class LogSerializer(serializers.ModelSerializer):
    # unique_ips
    # methods
    # total_bytes_sent
    class Meta:
        model = Logs
        fields = '__all__'

# class StatsSerializer(serializers.Serializer):
#     methods = serializers.SerializerMethodField()
#
#     def get_methods(self):

