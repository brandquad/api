from django.db import models


class HttpMethods(models.TextChoices):
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'
    PATCH = 'PATCH'
    DELETE = 'DELETE'
    HEAD = 'HEAD'
    OPTIONS = 'OPTIONS'
    CONNECT = 'CONNECT'
    TRACE = 'TRACE'


# В приложении должна быть модель, которая описывает распарсенные данные из лога. Поля модели должны содержать минимум:
# IP адрес, Дата из лога, http метод (GET, POST,...), URI запроса, Код ошибки, Размер ответа. Другие данные из лога -
# опциональны.
# todo Сделать уникальное поле
class Logs(models.Model):
    id = models.BigAutoField(primary_key=True)
    ip = models.GenericIPAddressField(blank=False, null=False)
    date = models.DateTimeField(blank=False, null=False)
    method = models.CharField(choices=HttpMethods.choices, max_length=10, blank=False, null=False)
    uri = models.URLField(max_length=2000)
    status_code = models.PositiveSmallIntegerField(blank=False, null=False)
    response_size = models.PositiveBigIntegerField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
