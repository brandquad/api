from rest_framework import routers
from django.urls import path
from logs.views import LogsViewSet, StatsView

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'logs', LogsViewSet, basename='logs')

urlpatterns = router.urls
urlpatterns.append(
    path('stats', StatsView.as_view(), name='stats')
)
