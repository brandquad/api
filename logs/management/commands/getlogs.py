from django.core.management.base import BaseCommand
import urllib.request
from lars import apache, progress
from logs.models import Logs
from smart_open import open


class Command(BaseCommand):
    help = "Заполняет базу данных логов"

    def add_arguments(self, parser):
        parser.add_argument('--url', type=str, required=True, help='URL лог-файла')
        parser.add_argument('--batch_size', type=int, default=1000, help='Количество записей лога, вставляемых в БД '
                                                                         'одним запросом')

    def handle(self, *args, **options):
        url = options.get('url')
        batch_size = options.get('batch_size')
        with open(url, 'r') as f:
            with progress.ProgressMeter(f) as meter:
                with apache.ApacheSource(f) as source:
                    target = OrmTarget(batch_size)
                    for row in source:
                        target.write(row)
                        # meter.update()


class OrmTarget:
    def __init__(self, batch_size=1000):
        self._count = 0
        self._batch_size = batch_size
        self._log_entries = []

    def write(self, row):
        log_entry = Logs(
            ip=row.remote_host.exploded,
            date=row.time,
            method=row.request.method,
            uri=f'{row.request.url.path_str}?{row.request.url.query_str}' if row.request.url.query_str
            else f'{row.request.url.path_str}',
            status_code=row.status,
            response_size=row.size,
        )
        self._log_entries.append(log_entry)
        self._count += 1
        if self._count >= self._batch_size:
            # todo Обработать ошибки?
            Logs.objects.bulk_create(self._log_entries)
            self._count = 0
            self._log_entries.clear()
